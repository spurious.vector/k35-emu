#ifndef VIDEO_H_
#define VIDEO_H_


#define SCREEN_WIDTH   200
#define SCREEN_HEIGHT  150
#define VRAM_LOC 0x500


int video_init(void);
int video_should_quit(void);
void video_prepare_screen(void);
void video_present_screen(void);


#endif
