#ifndef CPU_H_
#define CPU_H_

#include <stdint.h>


// Registers.
enum {
  // Qword registers (Register QWORD 0-7)
  R_RQ0,
  R_RQ1,
  R_RQ2,
  R_RQ3,
  R_RQ4,
  R_RQ5,
  R_RQ6,
  R_RQ7,
  R_RPC,          // Program counter.
  R_REG_COUNT
};


// Opcodes.
enum {
  OP_NOP = 0,
  OP_MOV = 1,
  OP_HLT = 2,
  OP_WRITES = 3,      // Writes RQ0 times.
};

void cpu_exec(void);

#endif
