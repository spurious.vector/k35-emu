#include <cpu.h>
#include <util.h>
#include <memory.h>
#include <video.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/prctl.h>
#include <sys/mman.h>

#define K35_DUMP_STATE

static uint8_t is_hlt = 0;
uint64_t regs[R_REG_COUNT];

static inline void shutdown_cycle(void) {
  _exit(1);
}


static inline void fault(void) {
  printf("[FATAL]: -- Fault caused shutdown cycle --\n");
  shutdown_cycle();
}

/*
 *  This function raises a fault
 *  if the pc argument (program counter) 
 *  is higher than there is memory.
 *
 *
 */

static inline void test_pc(uint64_t pc) {
  if (pc >= SIZEOF_ARRAY(memory)-20) {
    printf("[FAULT]: #GP\n");
    fault();
  }
}


#ifdef K35_DUMP_STATE
static const char* const REGS_STR[R_REG_COUNT] = {
  [R_RQ0] = "RQ0",
  [R_RQ1] = "RQ1",
  [R_RQ2] = "RQ2",
  [R_RQ3] = "RQ3",
  [R_RQ4] = "RQ4",
  [R_RQ5] = "RQ5",
  [R_RQ6] = "RQ6",
  [R_RQ7] = "RQ7",
  [R_RPC] = "RPC"
};

static void dump_state(void) {
  for (uint32_t i = 0; i < R_REG_COUNT; ++i) {
    if (i % 3 == 0)
      printf("\n");

    printf("%s=%014X ", REGS_STR[i], regs[i]);
  }

  printf("\n");
  printf("HLT=%d\n", is_hlt);
}
#endif


static void cycle(void) {
#ifdef K35_DUMP_STATE
  dump_state();
#endif
  uint64_t rpc = regs[R_RPC];
  test_pc(rpc);
  switch (memory[rpc]) {
    case OP_NOP:
      break;
    case OP_MOV:
      // MOV r, m64 (TODO add low registers soon)
      {
        uint16_t operand0 = memory[rpc + 1];

        if (memory[rpc + 3] == memory[rpc + 4]) {
          /*
           *  Register IDs are repeated twice
           *  in the binary so this must be a register.
           *
           */

          if (memory[rpc + 3] >= R_REG_COUNT) {
            printf("[FAULT]: INVALID OPCODE\n");
            fault();
          }
          uint16_t reg_src = memory[rpc + 3];
          regs[operand0] = regs[reg_src];
          ++regs[R_RPC];
        } else {
          regs[operand0] = memory[rpc + 3];
        }

        regs[R_RPC] += 3;
      }
      break;
    case OP_WRITES:
      {
        uint16_t addr = memory[rpc + 1];
        unsigned int value = memory[rpc + 2];
        while (regs[R_RQ0]) {
          memory[addr++] = value;
          --regs[R_RQ0];
        }

        regs[R_RPC] += 2;
      }
      break;
    case OP_HLT:
      is_hlt = 1;
#ifdef K35_DUMP_STATE
      dump_state();
#endif
      break;
    default:
      printf("[FAULT]: Invalid opcode (%x)\n", memory[rpc]);
      fault();
      break;
  }

  ++regs[R_RPC];
}


void cpu_exec(void) {
  video_prepare_screen(); 

  while (1) { 
    if (video_should_quit()) {
      break;
    }

    video_present_screen();
    if (!(is_hlt))
      cycle();
    else
      sleep(1);
  }
}
