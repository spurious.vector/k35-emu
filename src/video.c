#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdint.h>
#include <memory.h>
#include <video.h>


static SDL_Window* window;
static SDL_Renderer* renderer;


int video_init(void) {
  int window_flags = 0;
  int renderer_flags = SDL_RENDERER_ACCELERATED;

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("Failed to initialize video (SDL2): %s\n", SDL_GetError());
    return 1;
  }

  window = SDL_CreateWindow("K35Emu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, window_flags);

  if (window == NULL) {
    printf("Failed to open window (SDL2): %s\n", SDL_GetError());
    return 1;
  }

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
  renderer = SDL_CreateRenderer(window, -1, renderer_flags);

  if (renderer == NULL) {
    printf("Failed to create renderer (SDL2): %s\n", SDL_GetError());
    return 1;
  }

  return 0;
}


void video_prepare_screen(void) {
  SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
	SDL_RenderClear(renderer);
}


void video_present_screen(void) {
  for (uint32_t y = 0; y < SCREEN_HEIGHT; ++y) {
    for (uint32_t x = 0; x < SCREEN_WIDTH; ++x) {
      unsigned int color = memory[VRAM_LOC+(SCREEN_WIDTH*y+x)];
      SDL_SetRenderDrawColor(renderer, (color >> 16) & 0xFF, (color >> 8) & 0xFF, color & 0xFF, 255);
      SDL_RenderDrawPoint(renderer, x, y);
    }
  }

  SDL_RenderPresent(renderer);
}

int video_should_quit(void) {
  SDL_Event ev;
  while (SDL_PollEvent(&ev)) {
    switch (ev.type) {
      case SDL_QUIT:
        printf("Exiting..\n");
        SDL_Delay(16);
        return 1;
    }
  }

  SDL_Delay(16);
  return 0;
}
