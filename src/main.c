#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <cpu.h>
#include <util.h>
#include <memory.h>
#include <video.h>



unsigned int memory[MEMSIZE];


static void handle_arg(char* argv1) {
  FILE* fp = fopen(argv1, "r");

  if (fp == NULL) {
    perror("fopen");
    exit(1);
  }

  fseek(fp, 0, SEEK_END);
  size_t fsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  if (fsize > MEMSIZE-1) {
    printf("ERROR: Binary larger than max memory size.\n");
    fclose(fp);
    exit(1);
  }

  char* buf = calloc(fsize + 1, sizeof(char));
  fread(buf, sizeof(char), fsize, fp);
  fclose(fp);

  memcpy(memory, buf, fsize); 
  free(buf);
}


int main(int argc, char** argv) {
  if (argc < 2) {
    printf("ERROR: Too few arguments.\n");
    return 1;
  }

  if (argc > 2) {
    printf("ERROR: Too many arguments.\n");
    return 1;
  }

  handle_arg(argv[1]);

  if (video_init() != 0) {
    return 1;
  }

  cpu_exec();
  return 0;
}
