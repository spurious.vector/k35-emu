#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

// Opcodes.
enum {
  OP_NOP = 0,
  OP_MOV = 1,
  OP_HLT = 2,
  OP_WRITES = 3,      // Writes RQ0 times.
};

// Registers.
typedef enum {
  // Qword registers (Register QWORD 0-7)
  R_RQ0,
  R_RQ1,
  R_RQ2,
  R_RQ3,
  R_RQ4,
  R_RQ5,
  R_RQ6,
  R_RQ7,
  R_RPC,          // Program counter.
  R_REG_COUNT
} REG;


FILE* fp = NULL;


static void onexit(void) {
  if (fp != NULL)
    fclose(fp);
}


static REG reg2op(const char* regname) {
  if (strcmp(regname, "rq0") == 0) {
    return R_RQ0;
  } else if (strcmp(regname, "rq1") == 0) {
    return R_RQ1;
  } else if (strcmp(regname, "rq2") == 0) {
    return R_RQ2;
  } else if (strcmp(regname, "rq3") == 0) {
    return R_RQ3;
  } else if (strcmp(regname, "rq4") == 0) {
    return R_RQ4;
  } else if (strcmp(regname, "rq5") == 0) {
    return R_RQ5;
  } else if (strcmp(regname, "rq6") == 0) {
    return R_RQ6;
  } else if (strcmp(regname, "rq7") == 0) {
    return R_RQ7;
  }

  return R_REG_COUNT;
}


static void parse_line(const char* line) {
  char tmp[150];
  size_t tmpidx = 0;
  memset(tmp, 0, sizeof(tmp));

  for (uint32_t i = 0; i < 256; ++i) { 
    if (tmpidx >= sizeof(tmp)-1) {
      printf("ERROR: INVALID OPCODE/REGISTER.\n");
      break;
    }
  
    if (line[i] == ' ' || line[i] == '\0') { 
      if (strncmp(tmp, "nop", 3) == 0) {
        putw(OP_NOP, fp);
      } else if (strcmp(tmp, "hlt") == 0) {
        putw(OP_HLT, fp);
      } else if (strcmp(tmp, "mov") == 0) {
        putw(OP_MOV, fp);
      } else if (tmp[0] == '0' && tmp[1] == 'x') {
        putw((int)strtol(tmp, NULL, 16), fp);
      } else if (strcmp(tmp, "writes") == 0) {
        putw(OP_WRITES, fp);
      } else {
        REG r = reg2op(tmp);

        if (r == R_REG_COUNT) {
          printf("INVALID OPCODE/REGISTER\n");
          continue;
        }

        // Registers numbers are 
        // put in the binary twice.
        putw(r, fp);
        putw(r, fp);
      }

      memset(tmp, 0, sizeof(tmp));
      tmpidx = 0;
    }

    // Break if null terminator.
    if (!(line[i]))
      break;

    if (line[i] == ' ') continue;
    tmp[tmpidx++] = line[i];
    tmp[tmpidx] = '\0';           // Null terminate tmp.
  }
}


int main(void) {
  atexit(onexit);
  fp = fopen("out.k35", "w");
  char line[256];
  
  printf("Reading from stdin, use 'q' to save and exit.\n");
  while (fgets(line, sizeof(line), stdin) != NULL) { 
    if (line[0] == ';' || line[0] == '\n')
      continue;

    line[strcspn(line, "\n")] = '\0';
    line[strcspn(line, ";")] = '\0';

    if (strcmp(line, "q") == 0)
      break; 
  
    parse_line(line);
  }

  return 0;
}
