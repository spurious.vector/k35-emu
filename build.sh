fail() {
  cd ../
  rm -rf build
  exit 
}

mkdir -p bin
mkdir build
cd build
cmake ..
make || fail
cd ../
rm -rf build

